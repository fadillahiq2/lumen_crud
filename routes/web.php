<?php

/** @var \Laravel\Lumen\Routing\Router $router */
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/product', 'ProductController@index');
$router->post('/product', 'ProductController@create');
$router->post('/product/{id}', 'ProductController@update');
$router->post('/product/delete/{id}', 'ProductController@delete');



// $router->get('/foo', function () {
//     return 'method get!';
// });


// $router->post('/pos', function () {
//     return 'method post!';
// });

// $router->put('/put-method', function () {
//     return 'method put!';
// });

// $router->patch('/patch-method', function () {
//     return 'method patch';
// });

// $router->delete('/delete-method', function () {
//     return 'method delete';
// });

// //Basic Route Parameter
// $router->get('user/{id}', function ($id) {
//     return 'User id adalah : ' . $id;
// });

// $router->get('/posts/{postId}/comments/{commentId}', function ($postId, $commentId) {
//     return 'Post Id : ' . $postId . ', Comment Id : ' . $commentId;
// });

// $router->get('/optional[/{param}]', function ($param = 'Parameter masih kosong!') {
//     return $param;
// });

// //Router Prefix (group)
// $router->group(['prefix' => 'admin'], function () use ($router) {
//     $router->get('home', function () {
//         return 'Home admin!';
//     });

//     $router->get('profile', function () {
//         return 'Profile admin';
//     });
// });

// $router->get('hero/admin', ['as' => 'route-hero', function () {
//     return route('route-hero');
// }]);

// $router->get('/admin/home', ['middleware' => 'age', function () {
//     return 'Old enough/Berhasil';
// }]);

// $router->get('/fail', function () {
//     return 'Not yet mature/Gagal!';
// });

// $router->get('/hero/{id}', 'ExampleController@getHero');

// $router->get('/key', 'ExampleController@generateKey');

// $router->get('/cat/{cat1}/cat2/{cat2}', 'ExampleController@getCategory');
