<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getHero($id)
    {
        return 'User id : ' . $id;
    }

    public function generateKey()
    {
        return Str::random(32);
    }

    public function getCategory($cat1, $cat2)
    {
        return 'Category 1 : ' . $cat1 . ' Category 2 : ' . $cat2;
    }
}
