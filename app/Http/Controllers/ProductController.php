<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();

        if ($products == null)
        {
            $response['status'] = 'Fail';
            $response['message'] = 'Data Product kosong';

            return response()->json($response, 422);
        }

        $response['status'] = 'Success';
        $response['data'] = $products;

        return response()->json($response);
    }

    public function create(Request $request)
    {
        $name = $request->input('name');
        $price = $request->input('price');
        $category = $request->input('category');
        $photo = $request->input('photo_product');

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'photo_product' => 'required'
        ]);

        if ($validation->fails())
        {
            return response()->json([
                'status' => 'Error',
                'message' => $validation->errors()->first()
                ], 422
            );
        }

        Product::create([
            'name' => $name,
            'price' => $price,
            'category' => $category,
            'photo_product' => $photo
        ]);

        $response['status'] = 'Success';
        $response['message'] = 'Data Product Berhasil Disimpan!';

        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $name = $request->input('name');
        $price = $request->input('price');
        $category = $request->input('category');
        $photo = $request->input('photo_product');

        $validation = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'photo_product' => 'required'
        ]);

        if ($validation->fails())
        {
            return response()->json([
                'status' => 'Error',
                'message' => $validation->errors()->first()
                ], 422
            );
        }



        Product::where('id', $id)->update([
            'name' => $name,
            'price' => $price,
            'category' => $category,
            'photo_product' => $photo
        ]);

            $response['status'] = 'Success';
            $response['message'] = 'Data Product Berhasil Diupdate!';

            return response()->json($response);
    }

    public function delete($id)
    {
       $product = Product::where('id', $id)->delete();

       if ($product != null)
       {
        $response['status'] = 'Success';
        $response['message'] = 'Data Product Berhasil Dihapus!';
       }else {
        $response['status'] = 'Fail';
        $response['message'] = 'Data Product Gagal Dihapus!';
       }
    }

}
